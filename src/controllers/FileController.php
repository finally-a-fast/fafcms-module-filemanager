<?php
/**
 * @author    Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 Finally a fast
 * @license   https://www.finally-a-fast.com/packages/fafcms-module-filemanager/license MIT
 * @link      https://www.finally-a-fast.com/packages/fafcms-module-filemanager
 * @see       https://www.finally-a-fast.com/packages/fafcms-module-filemanager/docs Documentation of fafcms-module-filemanager
 * @since     File available since Release 1.0.0
 */

namespace fafcms\filemanager\controllers;

use Yii;
use fafcms\helpers\DefaultController;
use fafcms\filemanager\models\File;
use fafcms\filemanager\models\FileUploadForm;

/**
 * Class FileController
 *
 * @package fafcms\filemanager\controllers
 */
class FileController extends DefaultController
{
    public static $modelClass = File::class;

    /**
     * @return array|string
     * @throws \Exception
     */
    public function actionUpload()
    {
        $modelClass = FileUploadForm::class;

        return $this->renderActionContent(Yii::$app->view->renderView('model\\edit\\' . $modelClass, null, [
            'modelClass' => $modelClass,
            'createTitle' => ['fafcms-filemanager', 'Upload'],
            'addIndexToBreadcrumb' => false
        ]));
    }
}
