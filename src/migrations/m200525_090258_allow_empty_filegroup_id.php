<?php

namespace fafcms\filemanager\migrations;

use fafcms\filemanager\models\File;
use yii\db\Migration;

/**
 * Class m200525_090258_allow_empty_filegroup_id
 * @package fafcms\filemanager\migrations
 */
class m200525_090258_allow_empty_filegroup_id extends Migration
{
    public function safeUp()
    {
        $this->alterColumn(File::tableName(), 'filegroup_id', $this->integer(10)->unsigned()->null()->defaultValue(null));
    }

    public function safeDown()
    {
        $this->alterColumn(File::tableName(), 'filegroup_id', $this->integer(10)->unsigned()->notNull());
    }
}
