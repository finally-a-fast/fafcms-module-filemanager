<?php

namespace fafcms\filemanager\migrations;

use yii\db\Migration;

/**
 * Class m190924_143649_file_number
 * @package fafcms\filemanager\migrations
 */
class m190924_143649_file_number extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%file}}', 'number', $this->integer(10)->unsigned()->notNull()->defaultValue(0)->after('filename'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%file}}', 'number');
    }
}
