<?php

namespace fafcms\filemanager\migrations;

use fafcms\filemanager\models\Archive;
use fafcms\filemanager\models\File;
use fafcms\filemanager\models\Filetype;
use yii\db\Migration;

/**
 * Class m200212_191648_filetypes
 * @package fafcms\filemanager\migrations
 */
class m200212_191648_filetypes extends Migration
{
    public function safeUp()
    {
        $this->update(Filetype::tableName(), ['name' => 'WebP-Image'], ['id' => 6]);
        $this->update(Filetype::tableName(), ['name' => 'TIFF-Image'], ['id' => 7]);
    }

    public function safeDown()
    {
        $this->update(Filetype::tableName(), ['name' => 'WEBP'], ['id' => 6]);
        $this->update(Filetype::tableName(), ['name' => 'TIFF'], ['id' => 7]);
    }
}
