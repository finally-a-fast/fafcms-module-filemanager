<?php

namespace fafcms\filemanager\migrations;

use fafcms\filemanager\models\Archive;
use fafcms\filemanager\models\File;
use fafcms\filemanager\models\Filetype;
use yii\db\Migration;

/**
 * Class m200212_192133_filemeta
 * @package fafcms\filemanager\migrations
 */
class m200212_192133_filemeta extends Migration
{
    public function safeUp()
    {
        $this->addColumn(File::tableName(), 'meta', $this->text()->after('size'));
    }

    public function safeDown()
    {
        $this->dropColumn(File::tableName(), 'meta');
    }
}
