<?php

namespace fafcms\filemanager\migrations;

use fafcms\filemanager\models\File;
use fafcms\filemanager\models\Filegroup;
use fafcms\filemanager\models\Filetype;
use yii\db\Migration;

/**
 * Class m200120_205119_prefix
 * @package fafcms\filemanager\migrations
 */
class m200120_205119_prefix extends Migration
{
    public function safeUp()
    {
        $this->renameTable('{{%file}}', File::tableName());
        $this->renameTable('{{%filegroup}}', Filegroup::tableName());
        $this->renameTable('{{%filetype}}', Filetype::tableName());
    }

    public function safeDown()
    {
        $this->renameTable(File::tableName(), '{{%file}}');
        $this->renameTable(Filegroup::tableName(), '{{%filegroup}}');
        $this->renameTable(Filetype::tableName(), '{{%filetype}}');
    }
}
