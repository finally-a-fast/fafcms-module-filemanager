<?php

namespace fafcms\filemanager\migrations;

use fafcms\filemanager\models\File;
use fafcms\filemanager\models\Filesizevariation;
use fafcms\filemanager\models\Fileformatvariation;
use yii\db\Migration;

/**
 * Class m200204_083119_file_variation
 * @package fafcms\filemanager\migrations
 */
class m200204_083119_file_variation extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(Filesizevariation::tableName(), [
            'id' => $this->primaryKey(10)->unsigned(),
            'file_id' => $this->integer(10)->unsigned()->notNull(),
            'name' => $this->string(255)->notNull(),
            'height' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'width' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'allow_download' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(1),
            'is_public' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(1),
            'last_request_at' => $this->datetime()->null()->defaultValue(null),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'updated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
            'updated_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('idx-filesizevariation-file_id', Filesizevariation::tableName(), ['file_id'], false);
        $this->createIndex('idx-filesizevariation-created_by', Filesizevariation::tableName(), ['created_by'], false);
        $this->createIndex('idx-filesizevariation-updated_by', Filesizevariation::tableName(), ['updated_by'], false);

        $this->createTable(Fileformatvariation::tableName(), [
            'id' => $this->primaryKey(10)->unsigned(),
            'filesizevariation_id' => $this->integer(10)->unsigned()->notNull(),
            'variation' => $this->string(255)->notNull(),
            'last_request_at' => $this->datetime()->null()->defaultValue(null),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'updated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
            'updated_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('idx-fileformatvariation-filesizevariation_id', Fileformatvariation::tableName(), ['filesizevariation_id'], false);
        $this->createIndex('idx-fileformatvariation-created_by', Fileformatvariation::tableName(), ['created_by'], false);
        $this->createIndex('idx-fileformatvariation-updated_by', Fileformatvariation::tableName(), ['updated_by'], false);

        $this->addForeignKey('fk-filesizevariation-file_id', Filesizevariation::tableName(), 'file_id', File::tableName(), 'id', 'CASCADE', 'CASCADE');

        $this->addForeignKey('fk-fileformatvariation-filesizevariation_id', Fileformatvariation::tableName(), 'filesizevariation_id', Filesizevariation::tableName(), 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-filesizevariation-file_id', Filesizevariation::tableName());

        $this->dropForeignKey('fk-fileformatvariation-filesizevariation_id', Fileformatvariation::tableName());

        $this->dropTable(Filesizevariation::tableName());
        $this->dropTable(Fileformatvariation::tableName());
    }
}
