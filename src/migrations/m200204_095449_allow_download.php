<?php

namespace fafcms\filemanager\migrations;

use fafcms\filemanager\models\File;
use yii\db\Migration;

/**
 * Class m200204_095449_allow_download
 * @package fafcms\filemanager\migrations
 */
class m200204_095449_allow_download extends Migration
{
    public function safeUp()
    {
        $this->addColumn(File::tableName(), 'allow_download', $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(1)->after('size'));
        $this->alterColumn(File::tableName(), 'is_public', $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(1));
    }

    public function safeDown()
    {
        $this->dropColumn(File::tableName(), 'allow_download');
        $this->alterColumn(File::tableName(), 'is_public', $this->tinyInteger(1)->null()->defaultValue(0));
    }
}
