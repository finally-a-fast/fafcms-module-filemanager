<?php

namespace fafcms\filemanager\migrations;

use yii\db\Migration;

/**
 * Class m190827_000400_init
 * @package fafcms\sitemanager\migrations
 */
class m190827_000400_init extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%file}}', [
            'id' => $this->primaryKey(10)->unsigned(),
            'status' => $this->string(255)->notNull()->defaultValue('active'),
            'display_start' => $this->datetime()->null()->defaultValue(null),
            'display_end' => $this->datetime()->null()->defaultValue(null),
            'filegroup_id' => $this->integer(10)->unsigned()->notNull(),
            'filetype_id' => $this->integer(10)->unsigned()->notNull(),
            'filename' => $this->string(255)->notNull(),
            'alt' => $this->string(255)->null()->defaultValue(null),
            'size' => $this->integer(10)->unsigned()->notNull(),
            'is_public' => $this->tinyInteger(1)->null()->defaultValue(0),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'updated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'activated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deactivated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deleted_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
            'updated_at' => $this->datetime()->null()->defaultValue(null),
            'activated_at' => $this->datetime()->null()->defaultValue(null),
            'deactivated_at' => $this->datetime()->null()->defaultValue(null),
            'deleted_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('idx-file-filegroup_id', '{{%file}}', ['filegroup_id'], false);
        $this->createIndex('idx-file-filetype_id', '{{%file}}', ['filetype_id'], false);
        $this->createIndex('idx-file-created_by', '{{%file}}', ['created_by'], false);
        $this->createIndex('idx-file-updated_by', '{{%file}}', ['updated_by'], false);
        $this->createIndex('idx-file-activated_by', '{{%file}}', ['activated_by'], false);
        $this->createIndex('idx-file-deactivated_by', '{{%file}}', ['deactivated_by'], false);
        $this->createIndex('idx-file-deleted_by', '{{%file}}', ['deleted_by'], false);

        $this->createTable('{{%filegroup}}', [
            'id' => $this->primaryKey(10)->unsigned(),
            'status' => $this->string(255)->notNull()->defaultValue('active'),
            'display_start' => $this->datetime()->null()->defaultValue(null),
            'display_end' => $this->datetime()->null()->defaultValue(null),
            'parent_filegroup_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'name' => $this->string(255)->notNull(),
            'remarks' => $this->text()->null()->defaultValue(null),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'updated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'activated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deactivated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deleted_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
            'updated_at' => $this->datetime()->null()->defaultValue(null),
            'activated_at' => $this->datetime()->null()->defaultValue(null),
            'deactivated_at' => $this->datetime()->null()->defaultValue(null),
            'deleted_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('idx-filegroup-parent_filegroup_id', '{{%filegroup}}', ['parent_filegroup_id'], false);
        $this->createIndex('idx-filegroup-created_by', '{{%filegroup}}', ['created_by'], false);
        $this->createIndex('idx-filegroup-updated_by', '{{%filegroup}}', ['updated_by'], false);
        $this->createIndex('idx-filegroup-activated_by', '{{%filegroup}}', ['activated_by'], false);
        $this->createIndex('idx-filegroup-deactivated_by', '{{%filegroup}}', ['deactivated_by'], false);
        $this->createIndex('idx-filegroup-deleted_by', '{{%filegroup}}', ['deleted_by'], false);

        $this->createTable('{{%filetype}}', [
            'id' => $this->primaryKey(10)->unsigned(),
            'status' => $this->string(255)->notNull()->defaultValue('active'),
            'display_start' => $this->datetime()->null()->defaultValue(null),
            'display_end' => $this->datetime()->null()->defaultValue(null),
            'name' => $this->string(255)->null()->defaultValue(null),
            'mime_type' => $this->string(255)->notNull(),
            'default_extension' => $this->string(255)->notNull(),
            'mediatype' => $this->string(255)->notNull(),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'updated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'activated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deactivated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deleted_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
            'updated_at' => $this->datetime()->null()->defaultValue(null),
            'activated_at' => $this->datetime()->null()->defaultValue(null),
            'deactivated_at' => $this->datetime()->null()->defaultValue(null),
            'deleted_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('idx-filetype-created_by', '{{%filetype}}', ['created_by'], false);
        $this->createIndex('idx-filetype-updated_by', '{{%filetype}}', ['updated_by'], false);
        $this->createIndex('idx-filetype-activated_by', '{{%filetype}}', ['activated_by'], false);
        $this->createIndex('idx-filetype-deactivated_by', '{{%filetype}}', ['deactivated_by'], false);
        $this->createIndex('idx-filetype-deleted_by', '{{%filetype}}', ['deleted_by'], false);

        $this->addForeignKey('fk-file-filegroup_id', '{{%file}}', 'filegroup_id', '{{%filegroup}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('fk-file-filetype_id', '{{%file}}', 'filetype_id', '{{%filetype}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('fk-file-created_by', '{{%file}}', 'created_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-file-updated_by', '{{%file}}', 'updated_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-file-activated_by', '{{%file}}', 'activated_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-file-deactivated_by', '{{%file}}', 'deactivated_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-file-deleted_by', '{{%file}}', 'deleted_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');

        $this->addForeignKey('fk-filegroup-parent_filegroup_id', '{{%filegroup}}', 'parent_filegroup_id', '{{%filegroup}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-filegroup-created_by', '{{%filegroup}}', 'created_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-filegroup-updated_by', '{{%filegroup}}', 'updated_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-filegroup-activated_by', '{{%filegroup}}', 'activated_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-filegroup-deactivated_by', '{{%filegroup}}', 'deactivated_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-filegroup-deleted_by', '{{%filegroup}}', 'deleted_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');

        $this->batchInsert('{{%filetype}}',
            ['id', 'name', 'mime_type', 'default_extension', 'mediatype'],
            [
                [
                    'id' => '1',
                    'name' => 'JPEG-Image',
                    'mime_type' => 'image/jpeg',
                    'default_extension' => 'jpg',
                    'mediatype' => 'image',
                ],
                [
                    'id' => '2',
                    'name' => 'PNG-Image',
                    'mime_type' => 'image/png',
                    'default_extension' => 'png',
                    'mediatype' => 'image',
                ],
                [
                    'id' => '3',
                    'name' => 'BMP-Image',
                    'mime_type' => 'image/bmp',
                    'default_extension' => 'bmp',
                    'mediatype' => 'image',
                ],
                [
                    'id' => '4',
                    'name' => 'GIF-Image',
                    'mime_type' => 'image/gif',
                    'default_extension' => 'gif',
                    'mediatype' => 'image',
                ],
                [
                    'id' => '5',
                    'name' => 'SVG-Image',
                    'mime_type' => 'image/svg+xml',
                    'default_extension' => 'svg',
                    'mediatype' => 'image',
                ],
                [
                    'id' => '6',
                    'name' => 'WEBP',
                    'mime_type' => 'image/webp',
                    'default_extension' => 'webp',
                    'mediatype' => 'image',
                ],
                [
                    'id' => '7',
                    'name' => 'TIFF',
                    'mime_type' => 'image/tiff',
                    'default_extension' => 'tiff',
                    'mediatype' => 'image',
                ],
                [
                    'id' => '8',
                    'name' => 'Ogv-Video',
                    'mime_type' => 'video/ogg',
                    'default_extension' => 'ogv',
                    'mediatype' => 'video',
                ],
                [
                    'id' => '9',
                    'name' => 'MPEG-4-Video',
                    'mime_type' => 'video/mp4',
                    'default_extension' => 'mp4',
                    'mediatype' => 'video',
                ],
                [
                    'id' => '10',
                    'name' => 'QuickTime-Video',
                    'mime_type' => 'video/quicktime',
                    'default_extension' => 'mov',
                    'mediatype' => 'video',
                ],
                [
                    'id' => '11',
                    'name' => 'AVI-Video',
                    'mime_type' => 'video/x-msvideo',
                    'default_extension' => 'avi',
                    'mediatype' => 'video',
                ],
                [
                    'id' => '12',
                    'name' => 'Windows Media-Video',
                    'mime_type' => 'video/x-ms-wmv',
                    'default_extension' => 'wmv',
                    'mediatype' => 'video',
                ],
                [
                    'id' => '13',
                    'name' => '3GP Mobile-Video',
                    'mime_type' => 'video/3gpp',
                    'default_extension' => '3gp',
                    'mediatype' => 'video',
                ],
                [
                    'id' => '14',
                    'name' => 'Flash-Video',
                    'mime_type' => 'video/x-flv',
                    'default_extension' => 'flv',
                    'mediatype' => 'video',
                ],
                [
                    'id' => '15',
                    'name' => 'MPEG-Video',
                    'mime_type' => 'video/mpeg',
                    'default_extension' => 'mpg',
                    'mediatype' => 'video',
                ],
                [
                    'id' => '16',
                    'name' => 'M4V-Video',
                    'mime_type' => 'video/x-m4v',
                    'default_extension' => 'm4v',
                    'mediatype' => 'video',
                ],
                [
                    'id' => '17',
                    'name' => 'Adobe PDF',
                    'mime_type' => 'application/pdf',
                    'default_extension' => 'pdf',
                    'mediatype' => 'document',
                ],
                [
                    'id' => '18',
                    'name' => 'Rich-Text',
                    'mime_type' => 'application/rtf',
                    'default_extension' => 'rtf',
                    'mediatype' => 'document',
                ],
                [
                    'id' => '19',
                    'name' => 'Microsoft-Word',
                    'mime_type' => 'application/msword',
                    'default_extension' => 'doc',
                    'mediatype' => 'document',
                ],
                [
                    'id' => '20',
                    'name' => 'Microsoft-Word',
                    'mime_type' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                    'default_extension' => 'docx',
                    'mediatype' => 'document',
                ],
                [
                    'id' => '21',
                    'name' => 'OpenDocument Text',
                    'mime_type' => 'application/vnd.oasis.opendocument.text',
                    'default_extension' => 'odt',
                    'mediatype' => 'document',
                ],
                [
                    'id' => '22',
                    'name' => 'CSV',
                    'mime_type' => 'text/csv',
                    'default_extension' => 'csv',
                    'mediatype' => 'document',
                ],
                [
                    'id' => '23',
                    'name' => 'XLS',
                    'mime_type' => 'application/vnd.ms-office',
                    'default_extension' => 'xls',
                    'mediatype' => 'document',
                ],
                [
                    'id' => '24',
                    'name' => 'XLSX',
                    'mime_type' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                    'default_extension' => 'xlsx',
                    'mediatype' => 'document',
                ],
                [
                    'id' => '25',
                    'name' => 'ZIP-Archive',
                    'mime_type' => 'application/zip',
                    'default_extension' => 'zip',
                    'mediatype' => 'archive',
                ],
            ]
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-file-filegroup_id', '{{%file}}');
        $this->dropForeignKey('fk-file-filetype_id', '{{%file}}');
        $this->dropForeignKey('fk-file-created_by', '{{%file}}');
        $this->dropForeignKey('fk-file-updated_by', '{{%file}}');
        $this->dropForeignKey('fk-file-activated_by', '{{%file}}');
        $this->dropForeignKey('fk-file-deactivated_by', '{{%file}}');
        $this->dropForeignKey('fk-file-deleted_by', '{{%file}}');

        $this->dropForeignKey('fk-filegroup-parent_filegroup_id', '{{%filegroup}}');
        $this->dropForeignKey('fk-filegroup-created_by', '{{%filegroup}}');
        $this->dropForeignKey('fk-filegroup-updated_by', '{{%filegroup}}');
        $this->dropForeignKey('fk-filegroup-activated_by', '{{%filegroup}}');
        $this->dropForeignKey('fk-filegroup-deactivated_by', '{{%filegroup}}');
        $this->dropForeignKey('fk-filegroup-deleted_by', '{{%filegroup}}');

        $this->dropTable('{{%file}}');
        $this->dropTable('{{%filegroup}}');
        $this->dropTable('{{%filetype}}');
    }
}
