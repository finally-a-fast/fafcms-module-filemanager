<?php

namespace fafcms\filemanager\migrations;

use fafcms\filemanager\models\Archive;
use yii\db\Migration;

/**
 * Class m200205_155412_archive
 * @package fafcms\filemanager\migrations
 */
class m200205_155412_archive extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(Archive::tableName(), [
            'id' => $this->primaryKey(10)->unsigned(),
            'user_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'email' => $this->string(255)->null()->defaultValue(null),
            'files' => $this->text()->notNull(),
            'cron' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(1),
            'size' => $this->integer(10)->unsigned()->notNull()->defaultValue(0),
            'notifications_email' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'notificaitons_browser' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'generated' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'updated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
            'updated_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('idx-archive-user_id', Archive::tableName(), ['user_id'], false);
        $this->createIndex('idx-archive-created_by', Archive::tableName(), ['created_by'], false);
        $this->createIndex('idx-archive-updated_by', Archive::tableName(), ['updated_by'], false);
    }

    public function safeDown()
    {
        $this->dropTable(Archive::tableName());
    }
}
