<?php

namespace fafcms\filemanager;

use fafcms\fafcms\components\FafcmsComponent;
use yii\base\Application;
use fafcms\helpers\abstractions\PluginBootstrap;
use fafcms\helpers\abstractions\PluginModule;
use fafcms\filemanager\assets\FilemanagerAsset;
use fafcms\filemanager\assets\LazysizesAsset;
use fafcms\filemanager\assets\MediaAsset;
use Yii;
use yii\base\BootstrapInterface;
use yii\helpers\Url;
use yii\i18n\PhpMessageSource;
use Imagick;

class Bootstrap extends PluginBootstrap implements BootstrapInterface
{
    public static $id = 'fafcms-filemanager';
    public static $tablePrefix = 'fafcms-file_';

    protected function bootstrapTranslations(Application $app, PluginModule $module): bool
    {
        if (!isset($app->i18n->translations['fafcms-filemanager'])) {
            $app->i18n->translations['fafcms-filemanager'] = [
                'class' => PhpMessageSource::class,
                'basePath' => __DIR__ .'/messages',
                'forceTranslation' => true,
            ];
        }

        return true;
    }

    protected function bootstrapApp(Application $app, PluginModule $module): bool
    {
        $module->webp = false;

        if (class_exists('Imagick') && in_array('WEBP', Imagick::queryFormats())) {
            $module->webp = true;
        }

        return true;
    }

    protected function bootstrapWebApp(Application $app, PluginModule $module): bool
    {
        MediaAsset::register($app->view);
        LazysizesAsset::register($app->view);

        Yii::$app->request->noCsrfValidationRoutes[] = 'assets/archive';

        Yii::$app->view->addNavigationItems(
            FafcmsComponent::NAVIGATION_SIDEBAR_LEFT,
            [
                'files' => [
                    'after' => 'project',
                    'type' => 'group',
                    'label' => Yii::t('fafcms-filemanager', 'File Management'),
                    'items' => [
                        'files' => [
                            'icon' => 'file-multiple-outline',
                            'label' => Yii::t('fafcms-filemanager', 'Files'),
                            'url' => ['/' . $module->id . '/filemanager/index']
                        ],
                        'upload' => [
                            'icon' => 'upload-outline',
                            'label' => Yii::t('fafcms-filemanager', 'Upload'),
                            'url' => ['/' . $module->id . '/file/upload', 'filegroup_id' => $app->request->get('filegroup_id')],
                            'options' => [
                                'class' => 'ajax-modal',
                                'data' => [
                                    'ajax-modal-id' => 'fafcms-file-upload-modal',
                                ]
                            ]
                        ]
                    ],
                ],
            ]
        );

        $app->fafcms->addBackendUrlRules(self::$id, [
            '<controller:[a-zA-Z0-9\-]+>/<action:[a-zA-Z0-9\-]+>/<id:\d+>' => '<controller>/<action>',
            '<controller:[a-zA-Z0-9\-]+>/<action:[a-zA-Z0-9\-]+>' => '<controller>/<action>',
        ]);

        $app->fafcms->addFrontendUrlRules(self::$id, [
            'assets/<id:\d+>' => 'filemanager/get-file',
            'assets/pdf/<id:\d+>' => 'filemanager/view-pdf',
            'assets/pdf' => 'filemanager/view-pdf',
            'assets/archive' => 'filemanager/archive',
            'assets/archive-download' => 'filemanager/archive-download',
        ], false);

        $app->fafcms->addDisabledCookieManagerRoutes(self::$id, [
            'filemanager/view-pdf',
        ]);

        return true;
    }

    protected function bootstrapBackendApp(Application $app, PluginModule $module): bool
    {
        FilemanagerAsset::register($app->view);
        $app->params['body']['data-fafcms-file-upload-modal-url'] = Url::to(['/' . $module->id . '/file/upload', 'filegroup_id' => $app->request->get('filegroup_id')]);
        return true;
    }
}
