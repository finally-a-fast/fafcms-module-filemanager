<?php

namespace fafcms\filemanager\jobs;

use fafcms\filemanager\controllers\FilemanagerController;

/**
 * Class FileToImageJob
 *
 * @package fafcms\filemanager\jobs
 */
class FileToImageJob extends \yii\base\BaseObject implements \yii\queue\RetryableJobInterface
{
    /**
     * @var string
     */
    public $fileName;

    /**
     * @var string
     */
    public $fileFormatName;

    /**
     * @var string
     */
    public $sourceFormat;

    /**
     * @var array
     */
    public $type;

    /**
     * @var array
     */
    public $imageOptions;

    /**
     * @inheritdoc
     */
    public function execute($queue)
    {
        return FilemanagerController::generateImage($this->fileName, $this->fileFormatName, $this->sourceFormat, $this->type, $this->imageOptions);
    }

    /**
     * @inheritdoc
     */
    public function getTtr()
    {
        return 3 * (60 * 60);
    }

    /**
     * @inheritdoc
     */
    public function canRetry($attempt, $error)
    {
        return $attempt < 3;
    }
}
