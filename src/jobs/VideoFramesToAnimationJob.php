<?php

namespace fafcms\filemanager\jobs;

use Imagick;
use yii\helpers\StringHelper;

/**
 * Class VideoFramesToAnimationJob
 * @package fafcms\filemanager\jobs
 */
class VideoFramesToAnimationJob extends \yii\base\BaseObject implements \yii\queue\RetryableJobInterface
{
    public $rawFileWebpFormatName;
    public $rawFileGifFormatName;
    public $framePath;

    /**
     * @inheritdoc
     */
    public function execute($queue)
    {
        $gif = new Imagick();

        try {
            $files = scandir($this->framePath);
            sort($files, SORT_STRING | SORT_FLAG_CASE | SORT_NATURAL);

            $files = array_filter($files, static function ($file) {
                return StringHelper::endsWith($file, '.jpg');
            });

            foreach ($files as $i => $file) {
                if ($i > 92) {
                    break;
                }

                $frame = new Imagick();
                $frame->readImage($this->framePath.'/'.$file);
                $frame->setImageDelay(80);
                $gif->addImage($frame);
            }

            $gif->resetIterator();

            $gif->transformImageColorspace(Imagick::COLORSPACE_SRGB);
            $gif->setImageCompressionQuality(80);
            $gif->stripImage();
            $gif->setInterlaceScheme(Imagick::INTERLACE_PLANE);

            $gif = $gif->deconstructImages();

            if (!file_exists($this->rawFileGifFormatName)) {
                $gif->writeImages($this->rawFileGifFormatName,true);
            }

            if ($this->rawFileWebpFormatName !== null && !file_exists($this->rawFileWebpFormatName)) {
                $gif->writeImages($this->rawFileWebpFormatName,true);
            }
        } catch (\Exception $e) {
            $gif->clear();
        }
    }

    /**
     * @inheritdoc
     */
    public function getTtr()
    {
        return 3 * (60 * 60);
    }

    /**
     * @inheritdoc
     */
    public function canRetry($attempt, $error)
    {
        return $attempt < 3;
    }
}
