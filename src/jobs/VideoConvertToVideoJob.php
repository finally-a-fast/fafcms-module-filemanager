<?php

namespace fafcms\filemanager\jobs;

use fafcms\filemanager\controllers\FilemanagerController;
use fafcms\filemanager\models\File;
use fafcms\filemanager\models\Filetype;
use fafcms\filemanager\Module;
use FFMpeg\Coordinate\Dimension;
use FFMpeg\Coordinate\TimeCode;
use FFMpeg\FFMpeg;
use yii\log\Logger;

/**
 * Class VideoConvertToVideoJob
 * @package fafcms\filemanager\jobs
 */
class VideoConvertToVideoJob extends \yii\base\BaseObject implements \yii\queue\RetryableJobInterface
{
    public $fileId;
    public $fileFormatName;
    public $typeData;
    public $imageOptions;

    /**
     * @inheritdoc
     */
    public function execute($queue)
    {
        $file = File::find()->select([
                File::tableName().'.id',
                File::tableName().'.allow_download',
                File::tableName().'.is_public',
                File::tableName().'.filegroup_id',
                File::tableName().'.filetype_id',
                File::tableName().'.filename',
                Filetype::tableName().'.mediatype',
                Filetype::tableName().'.mime_type',
                Filetype::tableName().'.default_extension'
            ])
            ->innerJoinWith('filetype', false)
            ->where([
                File::tableName().'.id' => $this->fileId
            ])
            ->asArray()
            ->one();

        $fileName = File::getFilePath($file);
        $sourceFormat = $file['default_extension'];

        if (isset($this->imageOptions['format'])) {
            $file['default_extension'] = $this->imageOptions['format'];
        }

        $ffmpeg = FFMpeg::create(Module::getLoadedModule()->getFfmpegConfig());
        $video = $ffmpeg->open($fileName);

        //ffmpeg -i TEST.FILE -vcodec libx264 -b:v 600k -acodec libfdk_aac -ac 2 -ar 48000 -b:a 96k  -vf scale=480:-1 -map 0 out.mp4$video
        $video->filters()->resize(new Dimension(854, 480))->synchronize()->framerate(new FrameRate(50), 20);

        $format = new X264('aac');

        if ($sourceFormat === 'flv') {
            $format->setPasses(1);
        }

        /*$format->on('progress', function ($video, $format, $percentage) use ($file, $filepreviewParams) {
            if ($percentage % 5 === 0) {
                if ($this->_debug) {
                    echo "$percentage % transcoded";
                }

                $previewSettings = $this->getPreviewSettings($file, $filepreviewParams['name']);
                $previewSettings->{$filepreviewParams['name']}->progress = $percentage;

                $file->preview_settings = json_encode($previewSettings);
                $file->save();
            }
        });*/

        $video->save($format, $this->fileFormatName);
    }

    /**
     * @inheritdoc
     */
    public function getTtr()
    {
        return 3 * (60 * 60);
    }

    /**
     * @inheritdoc
     */
    public function canRetry($attempt, $error)
    {
        return $attempt < 3;
    }
}
