<?php

namespace fafcms\filemanager\widgets;

use fafcms\filemanager\Bootstrap;
use fafcms\filemanager\controllers\FilemanagerController;
use fafcms\filemanager\models\File;
use fafcms\filemanager\models\Filegroup;
use fafcms\filemanager\models\Filetype;
use fafcms\placeholder\Placeholder;
use Yii;
use yii\helpers\Url;
use yii\widgets\InputWidget;
use yii\helpers\Html;

class FileSelect extends InputWidget
{
    public $mediatypes = null;
    public $mimetypes = null;
    public ?int $limit = null;
    public bool $external = false;
    public ?string $selectButtonIcon = null;
    public ?array $selectButtonLabel = null;
    private $selectId;

    public function run()
    {
        $this->selectId = $this->id;

        Html::addCssClass($this->field->options, 'file-field');
        Html::addCssClass($this->options, 'fafcms-file-select-input');

        if ($this->hasModel()) {
            $attribute = $this->attribute;

            $this->selectId = $this->options['id'] ?? Html::getInputId($this->model, $attribute);
            $this->value = $this->options['value'] ?? Html::getAttributeValue($this->model, $attribute);

            $this->prepareInput();
            $content = Html::activeHiddenInput($this->model, $attribute, $this->options);
        } else {
            $this->prepareInput();
            $content = Html::hiddenInput($this->name, $this->value, $this->options);
        }

        $filegroup = null;
        $fileItems = '';

        if ($this->value !== null) {
            if ($this->limit === 1) {
                $filegroup = File::find()->select('filegroup_id')->where(['id' => $this->value])->scalar();
            }

            $fileItems .= FilemanagerController::getFileItems($this->value, $this->limit, $this->selectId);
        }

        if ($this->selectButtonIcon === null) {
            $this->selectButtonIcon = 'folder-multiple-image';
        }

        if ($this->selectButtonLabel === null) {
            if ($this->limit === 1) {
                $this->selectButtonLabel = ['fafcms-filemanager', 'Change file'];
            } else {
                $this->selectButtonLabel = ['fafcms-filemanager', 'Add files'];
            }
        }

        $content .= Html::tag('div', '<div class="file-items">' . $fileItems . '</div><div class="ui file-items-loader inverted dimmer"><div class="ui text loader">' . Yii::t('fafcms-core', 'Loading') . '</div></div>', ['id' => $this->selectId . '-file-items']);

        $content .= '<br>' . $this->render('@fafcms/fafcms/views/common/buttons', ['buttons' => [
            [
                'icon' => 'close-circle-multiple-outline',
                'label' => Yii::t('fafcms-filemanager', 'Remove all files'),
                'visible' => $this->limit > 1,
                'type' => 'button',
                'options' => [
                    'class' => 'ui button fafcms-file-select-remove',
                    'data-fafcms-file-select-id' => $this->selectId,
                ]
            ],
            [
                'icon' => $this->selectButtonIcon,
                'label' => Yii::$app->fafcms->getTextOrTranslation($this->selectButtonLabel),
                'type' => 'button',
                'options' => [
                    'id' => $this->selectId . '-button',
                    'class' => 'ui button ajax-modal primary',
                    'data' => [
                        'ajax-modal-id' => 'fafcms-file-select-modal',
                        'ajax-modal-url' => Url::to([
                            '/' . Bootstrap::$id . '/filemanager/select',
                            'FileSelectForm[id]' => $this->selectId,
                            'FileSelectForm[file]' => $this->value,
                            'FileSelectForm[mediatypes]' => $this->mediatypes,
                            'FileSelectForm[mimetypes]' => $this->mimetypes,
                            'FileSelectForm[limit]' => $this->limit,
                            'FileSelectForm[external]' => $this->external,
                            'FileSelectForm[filegroup]' => $filegroup,
                        ]),
                    ]
                ],
            ],
        ]]);

        return $content;
    }

    private function prepareInput(): void
    {
        $limit = 1;
        $this->options['data-fafcms-file-select-id'] = $this->selectId;

        if (is_array($this->value)) {
            $this->value = implode(',', $this->value);
            $limit = PHP_INT_MAX;
        }

        $this->options['value'] = $this->value;

        if ($this->limit === null) {
            $this->limit = $limit;
        }

        $this->options['data-fafcms-file-select-limit'] = $this->limit;
    }
}
