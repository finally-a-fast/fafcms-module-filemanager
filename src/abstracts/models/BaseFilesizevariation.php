<?php

namespace fafcms\filemanager\abstracts\models;

use fafcms\fafcms\{
    inputs\DateTimePicker,
    inputs\NumberInput,
    inputs\SwitchCheckbox,
    inputs\TextInput,
    items\ActionColumn,
    items\Card,
    items\Column,
    items\DataColumn,
    items\FormField,
    items\Row,
    items\Tab,
};
use fafcms\filemanager\{
    inputs\FileSelect,
    models\File,
    models\Fileformatvariation,
};
use fafcms\helpers\{
    ActiveRecord,
    interfaces\EditViewInterface,
    interfaces\FieldConfigInterface,
    interfaces\IndexViewInterface,
    traits\AttributeOptionTrait,
    traits\BeautifulModelTrait,
    traits\OptionTrait,
};
use Yii;
use yii\db\ActiveQuery;
use yii\validators\DateValidator;

/**
 * This is the abstract model class for table "{{%filesizevariation}}".
 *
 * @package fafcms\filemanager\abstracts\models
 *
 * @property-read array $fieldConfig
 *
 * @property int $id
 * @property int $file_id
 * @property string $name
 * @property int|null $height
 * @property int|null $width
 * @property int $allow_download
 * @property int $is_public
 * @property string|null $last_request_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property File $file
 * @property Fileformatvariation[] $filesizevariationFileformatvariations
 */
abstract class BaseFilesizevariation extends ActiveRecord implements FieldConfigInterface, IndexViewInterface, EditViewInterface
{
    use BeautifulModelTrait;
    use OptionTrait;
    use AttributeOptionTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return 'filesizevariation';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return  'filesizevariation';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('fafcms-file', 'Filesizevariations');
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('fafcms-file', 'Filesizevariation');
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['name'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region OptionTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptions(bool $empty = true, array $select = null, array $sort = null, array $where = null, array $joinWith = null, string $emptyLabel = null): array
    {
        $options = Yii::$app->dataCache->map(
            static::class,
            array_merge([
                static::tableName() . '.id',
                static::tableName() . '.name'
            ], $select ?? []),
            'id',
            static function ($item) {
                return static::extendedLabel($item);
            },
            null,
            $sort ?? [static::tableName() . '.name' => SORT_ASC],
            $where,
            $joinWith
        );

        return OptionTrait::getOptions($empty, $select, $sort, $where, $joinWith, $emptyLabel) + $options;
    }
    //endregion OptionTrait implementation

    //region AttributeOptionTrait implementation
    /**
     * @inheritDoc
     */
    public function attributeOptions(): array
    {
        return [
            'file_id' => static function(...$params) {
                return File::getOptions(...$params);
            },
        ];
    }
    //endregion AttributeOptionTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return [
            'id' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'file_id' => [
                'type' => FileSelect::class,
                'external' => false,
            ],
            'name' => [
                'type' => TextInput::class,
            ],
            'height' => [
                'type' => NumberInput::class,
            ],
            'width' => [
                'type' => NumberInput::class,
            ],
            'allow_download' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'is_public' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'last_request_at' => [
                'type' => DateTimePicker::class,
            ],
            'created_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'created_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
        ];
    }
    //endregion FieldConfigInterface implementation

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        return [
            'default' => [
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'id',
                        'sort' => 1,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'file_id',
                        'sort' => 3,
                        'link' => true,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'name',
                        'sort' => 3,
                        'link' => true,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'height',
                        'sort' => 4,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'width',
                        'sort' => 5,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'allow_download',
                        'sort' => 6,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'is_public',
                        'sort' => 7,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'last_request_at',
                        'sort' => 8,
                    ],
                ],
                [
                    'class' => ActionColumn::class,
                ],
            ]
        ];
    }
    //endregion IndexViewInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-file_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'file_id',
                                                    ],
                                                ],
                                                'field-name' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'name',
                                                    ],
                                                ],
                                                'field-height' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'height',
                                                    ],
                                                ],
                                                'field-width' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'width',
                                                    ],
                                                ],
                                                'field-allow_download' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'allow_download',
                                                    ],
                                                ],
                                                'field-is_public' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'is_public',
                                                    ],
                                                ],
                                                'field-last_request_at' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'last_request_at',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%filesizevariation}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'required-file_id' => ['file_id', 'required'],
            'required-name' => ['name', 'required'],
            'integer-file_id' => ['file_id', 'integer'],
            'integer-height' => ['height', 'integer'],
            'integer-width' => ['width', 'integer'],
            'integer-created_by' => ['created_by', 'integer'],
            'integer-updated_by' => ['updated_by', 'integer'],
            'boolean-allow_download' => ['allow_download', 'boolean'],
            'boolean-is_public' => ['is_public', 'boolean'],
            'date-last_request_at' => ['last_request_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'last_request_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-created_at' => ['created_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'created_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-updated_at' => ['updated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'updated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'string-name' => ['name', 'string', 'max' => 255],
            'exist-file_id' => [['file_id'], 'exist', 'skipOnError' => true, 'targetClass' => File::class, 'targetAttribute' => ['file_id' => 'id']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'id' => Yii::t('fafcms-file', 'ID'),
            'file_id' => Yii::t('fafcms-file', 'File ID'),
            'name' => Yii::t('fafcms-file', 'Name'),
            'height' => Yii::t('fafcms-file', 'Height'),
            'width' => Yii::t('fafcms-file', 'Width'),
            'allow_download' => Yii::t('fafcms-file', 'Allow Download'),
            'is_public' => Yii::t('fafcms-file', 'Is Public'),
            'last_request_at' => Yii::t('fafcms-file', 'Last Request At'),
            'created_by' => Yii::t('fafcms-file', 'Created By'),
            'updated_by' => Yii::t('fafcms-file', 'Updated By'),
            'created_at' => Yii::t('fafcms-file', 'Created At'),
            'updated_at' => Yii::t('fafcms-file', 'Updated At'),
        ]);
    }

    /**
     * Gets query for [[File]].
     *
     * @return ActiveQuery
     */
    public function getFile(): ActiveQuery
    {
        return $this->hasOne(File::class, [
            'id' => 'file_id',
        ]);
    }

    /**
     * Gets query for [[FilesizevariationFileformatvariations]].
     *
     * @return ActiveQuery
     */
    public function getFilesizevariationFileformatvariations(): ActiveQuery
    {
        return $this->hasMany(Fileformatvariation::class, [
            'filesizevariation_id' => 'id',
        ]);
    }
}
