<?php

use fafcms\filemanager\Bootstrap;
use yii\helpers\Url;
use yii\helpers\Html;
use fafcms\fafcms\widgets\Breadcrumbs;
use fafcms\filemanager\models\File;
use fafcms\filemanager\models\Filegroup;

if (!isset($select)) {
    $select = false;
}

if ($select) {
    echo Html::tag(
        'header',
        Html::tag('h4', $title).
        Html::tag(
            'div',
            Breadcrumbs::widget([
                'links' => $breadcrumb,
                'homeLink' => $homeLink
            ]),
            ['class' => 'ui vertical inverted segment breadcrumb-header']
        ),
        ['class' => 'modal-header']
    );
}

$columns = [];

foreach ($filegroups as $filegroup) {
    $filegroupName = htmlentities($filegroup['name']);

    $fileCountQuery = clone $fileQuery;
    $filegroupCountQuery = clone $filegroupQuery;

    $fileCount = $fileCountQuery->andWhere(['filegroup_id' => $filegroup['id']])->count();
    $filegroupCount = $filegroupCountQuery->andWhere(['parent_filegroup_id' => $filegroup['id']])->count();

    $item = Html::beginTag('a', [
        'href' => Url::to($select ? [
            '/' . Bootstrap::$id . '/filemanager/select',
            'FileSelectForm[id]' => $model->id,
            'FileSelectForm[file]' => $model->file,
            'FileSelectForm[mediatypes]' => $model->mediatypes,
            'FileSelectForm[mimetypes]' => $model->mimetypes,
            'FileSelectForm[limit]' => $model->limit,
            'FileSelectForm[external]' => $model->external,
            'FileSelectForm[filegroup]' => $filegroup['id'],
        ]:[
            'filemanager/index',
            'id' => $filegroup['id'],
        ]),
        'class' => [
            'filemanager-list-item-view',
            ($select ? 'ajax-modal' : ''),
        ],
        'data' => ($select ? [
            'ajax-modal-id' => 'fafcms-file-select-modal',
        ] : [])
    ]);

    $item .= Html::tag('div', '<i class="filemanager-list-item-closed mdi mdi-' . ($fileCount > 0 || $filegroupCount > 0?'folder':'folder-outline') . '"></i><i class="filemanager-list-item-open mdi mdi-' . ($fileCount > 0 || $filegroupCount > 0?'folder-open':'folder-open-outline') . '"></i>', [
        'class' => 'filemanager-list-item-icon'
    ]);

    $item .= Html::endTag('a');

    $item .= $this->render('_item', [
        'name' => $filegroupName,
        'badges' => [[
            'icon' => File::instance()->getEditData()['icon'],
            'count' => $fileCount
        ], [
            'icon' => Filegroup::instance()->getEditData()['icon'],
            'count' => $filegroupCount
        ]],
        'editLink' => [Filegroup::instance()->getEditDataUrl() . '/update', 'id' => $filegroup['id']],
        'deleteLink' => [Filegroup::instance()->getEditDataUrl() . '/delete', 'id' => $filegroup['id']],
        'downloadLink' => null
    ]);

    $options = [
        'class' => 'ui segment center aligned filemanager-list-item'
    ];

    $columns[] = [
        'options' => ['class' => ['sixteen wide mobile eight wide tablet four wide computer two wide large screen one wide widescreen']],
        'content' => Html::tag('div', $item, $options ?? []),
    ];
}

foreach ($files as $file) {
    $fileName = htmlentities(File::getFileName($file));

    $item = Html::beginTag('a', [
        'href' => Url::to($select ? [
            '/' . Bootstrap::$id . '/filemanager/select',
            'FileSelectForm[id]' => $model->id,
            'FileSelectForm[file]' => $file['id'],
            'FileSelectForm[mediatypes]' => $model->mediatypes,
            'FileSelectForm[mimetypes]' => $model->mimetypes,
            'FileSelectForm[limit]' => $model->limit,
            'FileSelectForm[external]' => $model->external,
            'FileSelectForm[filegroup]' => $model->filegroup,
        ] : [
            File::instance()->getEditDataUrl() . '/update',
            'id' => $file['id']
        ]),
        'class' => [
            'filemanager-list-item-view',
            ($select ? 'ajax-modal' : ''),
        ],
        'data' => ($select ? [
            'ajax-modal-id' => 'fafcms-file-select-modal',
        ] : [])
    ]);

    if ($file['mediatype'] === 'image') {
        $item .= File::getPicture($file, 'Filemanager preview', [
            [500, 500, 'bestfit:true;'],
            [75, 75, 'bestfit:true;']
        ], false, null);
    } else {
        $item .= Html::tag('div', '<i class="mdi mdi-file"></i>', ['class' => 'filemanager-list-item-icon']);
    }

    $item .= Html::endTag('a');

    $item .= $this->render('_item', [
        'name' => $fileName,
        'badges' => [[
            'icon' => 'harddisk',
            'count' => Yii::$app->formatter->asShortSize($file['size'], 2)
        ], [
            'icon' => 'file-question-outline',
            'count' => $file['default_extension']
        ]],
        'editLink' => [File::instance()->getEditDataUrl() . '/update', 'id' => $file['id']],
        'deleteLink' => [File::instance()->getEditDataUrl() . '/delete', 'id' => $file['id']],
        'downloadLink' => [Filegroup::instance()->getEditDataUrl() . '/get-file', 'id' => $file['id'], 'download' => 'true'],
    ]);

    $options = [
        'class' => 'ui segment center aligned filemanager-list-item'
    ];

    if ($select && $model->file === $file['id']) {
        Html::addCssClass($options, 'blue secondary');
    }

    $columns[] = [
        'options' => ['class' => [
            'sixteen wide mobile eight wide tablet four wide computer two wide large screen one wide widescreen',
        ]],
        'content' => Html::tag('div', $item, $options ?? []),
    ];
}

echo $this->render(
    '@fafcms/fafcms/views/common/grid', [
        'rows' => [$columns],
  ]
);
