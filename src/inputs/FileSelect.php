<?php

namespace fafcms\filemanager\inputs;

use fafcms\filemanager\widgets\FileSelect as FileSelectWidget;
use fafcms\helpers\abstractions\FormInput;

/**
 * Class FileSelect
 * @package fafcms\filemanager\inputs
 */
class FileSelect extends FormInput
{
    public $mediatypes = null;
    public $mimetypes = null;
    public ?int $limit = null;
    public bool $external = false;
    public ?string $selectButtonIcon = null;
    public ?array $selectButtonLabel = null;

    public function run(): string
    {
        $this->options['mediatypes'] = $this->mediatypes;
        $this->options['mimetypes'] = $this->mimetypes;
        $this->options['limit'] = $this->limit;
        $this->options['external'] = $this->external;
        $this->options['selectButtonIcon'] = $this->selectButtonIcon;
        $this->options['selectButtonLabel'] = $this->selectButtonLabel;

        return $this->form->field($this->model, $this->name)
            ->label(null, $this->labelOptions)
            ->hint($this->description)
            ->widget(FileSelectWidget::class, $this->getInputOptions(true));
    }
}
