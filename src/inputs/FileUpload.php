<?php
/**
 * @author    Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 Finally a fast
 * @license   https://www.finally-a-fast.com/packages/fafcms-module-filemanager/license MIT
 * @link      https://www.finally-a-fast.com/packages/fafcms-module-filemanager
 * @see       https://www.finally-a-fast.com/packages/fafcms-module-filemanager/docs Documentation of fafcms-module-filemanager
 * @since     File available since Release 1.0.0
 */

namespace fafcms\filemanager\inputs;

use fafcms\helpers\abstractions\FormInput;

/**
 * Class TextInput
 * @package fafcms\filemanager\inputs
 */
class FileUpload extends FormInput
{
    public $multiple = true;
    public $fallbackButtonLabel;
    public $fallbackButtonLabelOptions = [];
    public $maxFiles;
    public $maxFileSize;
    public $acceptedFileExtensions;

    public function run(): string
    {
        $widgetOptions = array_merge($this->options, [
            'fallbackButtonLabel' => $this->fallbackButtonLabel,
            'fallbackButtonLabelOptions' => $this->fallbackButtonLabelOptions,
            'maxFiles' => $this->maxFiles,
            'maxFileSize' => $this->maxFileSize,
            'acceptedFileExtensions' => $this->acceptedFileExtensions,
            'multiple' => $this->multiple,
            'formId' => $this->form->id
        ]);

        return $this->form->field($this->model, $this->name, ['template' => "{icon}\n{label}\n{input}\n{hint}\n{error}"])
            ->label(null, $this->labelOptions)
            ->hint($this->description)
            ->widget(\fafcms\filemanager\widgets\FileUpload::class, $widgetOptions);
    }
}
