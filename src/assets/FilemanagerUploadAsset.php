<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 - 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-filemanager/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-filemanager
 * @see https://www.finally-a-fast.com/packages/fafcms-filemanager/docs Documentation of fafcms-filemanager
 * @since File available since Release 1.0.0
 */

declare(strict_types=1);

namespace fafcms\filemanager\assets;

use fafcms\fafcms\assets\fafcms\backend\FafcmsAjaxModalAsset;
use fafcms\helpers\classes\AssetComponentBundle;

/**
 * Class FilemanagerUploadAsset
 *
 * @package fafcms\filemanager\assets
 */
class FilemanagerUploadAsset extends AssetComponentBundle
{
    public $sourcePath = __DIR__ . '/filemanager-upload';

    public $js = [
        'filemanager-upload.js',
    ];

    public $css = [
        'filemanager-upload.scss',
    ];

    public $depends = [
        DropzoneAsset::class,
        FafcmsAjaxModalAsset::class,
    ];
}
