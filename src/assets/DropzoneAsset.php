<?php

namespace fafcms\filemanager\assets;

use yii\web\AssetBundle;

class DropzoneAsset extends AssetBundle
{
    public $sourcePath = '@vendor/enyo/dropzone/dist/min';

    public $js = [
        'dropzone.min.js',
    ];
}
