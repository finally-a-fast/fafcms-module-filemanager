function FilemanagerSelect() {
  let cmsModule = this
  let fafApp = window.faf.app
  let fafToast = window.faf.FafcmsToast

  let updateFileItems = function(id) {
    let input = document.getElementById(id)
    document.querySelector('#' + id + '-file-items .file-items-loader').classList.add('active')

    $.get('/' + window.faf.config.backend + '/fafcms-filemanager/filemanager/get-file-items', {
      'fileIds': input.value,
      'limit': input.dataset.fafcmsFileSelectLimit,
      'fileSelectId': id
    }).done(function(response) {
      let fileItems = document.querySelector('#' + id + '-file-items .file-items')
      fileItems.innerHTML = response
      document.querySelector('#' + id + '-file-items .file-items-loader').classList.remove('active')
      fafApp.runEvents(window.faf.events, 'initPlugins', fileItems)
    }).fail(function (e) {
      if (typeof e.responseJSON.message !== 'undefined') {
        fafToast.toast({message: e.responseJSON.message, type: 'error'})
      } else {
        fafToast.toast({message: fafApp.t('fafcms-core', 'An error has occurred, please try again.'), type: 'error'})
      }
    })
  }

  let initFileSelectInput = function(target) {
    let elements = window.faf.app.findChildren(target, '.fafcms-file-select-input:not(.fafcms-file-select-input-initialized)')

    for (let i = 0; i < elements.length; i++) {
      elements[i].addEventListener('change', function (e) {
        updateFileItems($(this).data('fafcms-file-select-id'))
      })

      elements[i].classList.add('fafcms-file-select-input-initialized')
    }
  }

  let initFileSelectRemove = function(target) {
    let elements = window.faf.app.findChildren(target, '.fafcms-file-select-remove:not(.fafcms-file-select-remove-initialized)')

    for (let i = 0; i < elements.length; i++) {
      elements[i].addEventListener('click', function (e) {
        e.preventDefault()

        let dataset = this.dataset
        let id = dataset.fafcmsFileSelectId
        let file = dataset.fafcmsFileSelectFile

        let input = document.getElementById(id)

        if (typeof file === 'undefined') {
          input.value = ''
          fafToast.toast({message: fafApp.t('fafcms-filemanager', 'All files have been removed.'), type: 'success'})
        } else {
          let newValue = input.value.split(',')

          newValue = newValue.filter(function(item) {
            return item !== file
          })

          input.value = newValue.join(',')
          fafToast.toast({message: fafApp.t('fafcms-filemanager', 'File has been removed.'), type: 'success'})
        }

        updateFileItems(id)
      })

      elements[i].classList.add('fafcms-file-select-remove-initialized')
    }
  }

  let initFileSelectApply = function(target) {
    let elements = window.faf.app.findChildren(target, '.fafcms-file-select-apply:not(.fafcms-file-select-apply-initialized)')

    for (let i = 0; i < elements.length; i++) {
      elements[i].addEventListener('click', function (e) {
        e.preventDefault()

        let dataset = this.dataset
        let id = dataset.fafcmsFileSelectId
        let file = dataset.fafcmsFileSelectFile

        let input = document.getElementById(id)

        if (input.dataset.fafcmsFileSelectLimit === '1') {
          input.value = file
          $('#fafcms-file-select-modal').modal('hide')
          fafToast.toast({message: fafApp.t('fafcms-filemanager', 'File has been applied.'), type: 'success'})
        } else {
          input.value += (input.value !== '' ? ',' : '') + file
          fafToast.toast({message: fafApp.t('fafcms-filemanager', 'File has been added.'), type: 'success'})
        }

        document.getElementById(id + '-button').dataset.ajaxModalUrl = dataset.fafcmsFileSelectUrl
        updateFileItems(id)
      })

      elements[i].classList.add('fafcms-file-select-apply-initialized')
    }
  }

  cmsModule.init = function () {
    initFileSelectApply(document.getElementsByTagName('html'))
    initFileSelectInput(document.getElementsByTagName('html'))
    initFileSelectRemove(document.getElementsByTagName('html'))
  }

  window.faf.events['initPlugins'].push(function (target) {
    initFileSelectApply(target)
    initFileSelectInput(target)
    initFileSelectRemove(target)
  })
}
