<?php
namespace fafcms\filemanager\assets;

use yii\web\AssetBundle;
use fafcms\fafcms\components\ViewComponent;

class LazysizesAsset extends AssetBundle
{
    public $sourcePath = '@bower/lazysizes';

    public $js = [
        'lazysizes.min.js',
        'plugins/respimg/ls.respimg.min.js',
        'plugins/custommedia/ls.custommedia.min.js',
    ];

    public $jsOptions = [
        'position' => ViewComponent::POS_HEAD
    ];
}
