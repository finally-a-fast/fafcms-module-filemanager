<?php
namespace fafcms\filemanager\models;

use fafcms\filemanager\Bootstrap;
use fafcms\filemanager\controllers\FilemanagerController;
use yii\base\Model;
use yii\db\Expression;
use Yii;
use yii\web\BadRequestHttpException;

class ArchiveForm extends Model
{
    public $files;
    public $archive;

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['files'], 'required'],
            [['files'], 'string'],
        ];
    }

    public function save($runValidation = true, $attributeNames = null): bool
    {
        $requestedFiles = explode(',', $this->files);

        if (count($requestedFiles) > 0) {
            $this->archive = new Archive();
            $this->archive->generated = 0;

            if (!Yii::$app->getUser()->getIsGuest()) {
                $this->archive->user_id = Yii::$app->getUser()->getId()??null;
                $this->archive->email = Yii::$app->getUser()->getIdentity()->email??null;
            }

            $this->archive->created_at = (new \DateTime('NOW', new \DateTimeZone(Yii::$app->formatter->defaultTimeZone)))->format('Y-m-d H:i:s');
            $this->archive->size = 0;

            $archiveFiles = [];

            foreach ($requestedFiles as $requestedFile) {
                $variation = Yii::$app->security->validateData($requestedFile, Yii::$app->getModule(Bootstrap::$id)->getPluginSettingValue('file_variation_security_key'));

                if ($variation === false) {
                    $file = File::find()
                        ->select('id, size')
                        ->where(['hashId' => $requestedFile])
                        ->asArray()
                        ->one();

                    if ($file !== null) {
                        $this->archive->size += $file['size'];

                        $archiveFiles[] = [
                            'id' => $file['id']
                        ];
                    }
                } else {
                    if ($variation === false ||
                        ($variation = base64_decode($variation)) === false ||
                        ($variation = json_decode($variation, false)) === null ||
                        ($file = File::find()
                            ->where(['hashId' => $variation->id])
                            ->one()) === null) {
                        throw new BadRequestHttpException('File is invalid!');
                    }

                    $size = $file->size;

                    $archiveFile = [
                        'id' => $file['id']
                    ];

                    if (isset($variation->type)) {
                        $fileVariation = FilemanagerController::getFileVariation($variation->type, $file);

                        if ($fileVariation === null) {
                            continue;
                        }

                        $meta = File::loadMeta($file, $fileVariation);
                        $size = $meta['size'];

                        $archiveFile['type'] = $variation->type;
                        $archiveFile['name'] = $variation->name;
                    }

                    $this->archive->size += $size;
                    $archiveFiles[] = $archiveFile;
                }
            }

            $this->archive->cron = 0;

            if ($this->archive->size > 3.9 * 1024 * 1024 * 1024) {
                $this->archive->cron = 1;
            }

            $this->archive->files = json_encode($archiveFiles);
            return $this->archive->save();
        }

        return false;
    }
}
