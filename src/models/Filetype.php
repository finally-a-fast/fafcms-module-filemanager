<?php

namespace fafcms\filemanager\models;

use fafcms\filemanager\abstracts\models\BaseFiletype;
use fafcms\filemanager\Bootstrap;
use fafcms\helpers\traits\OptionTrait;
use Yii;

/**
 * This is the model class for table "{{%filetype}}".
 *
 * @package fafcms\filemanager\models
 */
class Filetype extends BaseFiletype
{
    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return Bootstrap::$id . '/filetype';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return 'file-question-outline';
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['name'] ?? '').' (.'.($model['default_extension'] ?? '').')');
    }
    //endregion BeautifulModelTrait implementation

    //region OptionTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptions(bool $empty = true, array $select = null, array $sort = null, array $where = null, array $joinWith = null, string $emptyLabel = null): array
    {
        $options = Yii::$app->dataCache->map(
            static::class,
            array_merge([
                static::tableName() . '.id',
                static::tableName() . '.name',
                static::tableName() . '.default_extension'
            ], $select??[]),
            'id',
            static function ($item) {
                return static::extendedLabel($item);
            },
            null,
            $sort ?? [static::tableName() . '.name' => SORT_ASC],
            $where,
            $joinWith
        );

        return OptionTrait::getOptions($empty, $select, $sort, $where, $joinWith, $emptyLabel) + $options;
    }
    //endregion OptionTrait implementation
}
