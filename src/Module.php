<?php

namespace fafcms\filemanager;

use fafcms\filemanager\inputs\FileSelect;
use Yii;
use yii\web\UploadedFile;
use fafcms\fafcms\components\FafcmsComponent;
use fafcms\fafcms\inputs\NumberInput;
use fafcms\fafcms\inputs\ExtendedDropDownList;
use fafcms\fafcms\inputs\SwitchCheckbox;
use fafcms\fafcms\inputs\Textarea;
use fafcms\fafcms\inputs\TextInput;
use fafcms\filemanager\models\Filetype;
use fafcms\filemanager\models\FileUploadForm;
use fafcms\filemanager\models\Filegroup;
use fafcms\helpers\abstractions\PluginModule;
use fafcms\helpers\classes\PluginSetting;

class Module extends PluginModule
{
    public $controllerNamespace = 'fafcms\filemanager\controllers';
    public $webp;
    private $_validationKey;

    public function getValidationKey()
    {
        if ($this->_validationKey === null) {
            return Yii::$app->getModule(Bootstrap::$id)->getPluginSettingValue('file_variation_security_key');
        }

        return $this->_validationKey;
    }

    public function setValidationKey($key)
    {
        $this->_validationKey = $key;
    }

    /**
     * @return string
     */
    public function getImagineClass(): string
    {
        switch (Yii::$app->getModule(Bootstrap::$id)->getPluginSettingValue('image_manipulation_library')) {
            case 3:
                return 'Imagine\Gd\Imagine';
            case 2:
                return 'Imagine\Gmagick\Imagine';
            default:
                return 'Imagine\Imagick\Imagine';
        }
    }

    /**
     * @return \Imagine\Image\AbstractImagine
     */
    public function getImagine(): \Imagine\Image\AbstractImagine
    {
        $imagineClass = $this->getImagineClass();
        return new $imagineClass;
    }

    public function getGetFilegroupByPath($path, $create = true)
    {
        $groupNames = explode('/', $path);

        foreach ($groupNames as $groupName) {
            $parentFilegroupId = $filegroup->id??null;

            $filegroup = Filegroup::find()->where([
                'name' => $groupName,
                'parent_filegroup_id' => $parentFilegroupId
            ])->one();

            if ($filegroup === null) {
                if ($create) {
                    $filegroup = new Filegroup();
                    $filegroup->name = $groupName;
                    $filegroup->parent_filegroup_id = $parentFilegroupId;
                    $filegroup->save();
                }
                else {
                    return false;
                }
            }
        }

        return $filegroup;
    }

    public function getPluginSettingRules(): array
    {
        return [
            [['file_variation_security_key', 'default_upload_folder_new_path', 'default_upload_filename_format', 'default_thumbnail_background_image', 'default_thumbnail_inner_background_image'], 'string'],
            [['generate_images_on_the_fly', 'force_youtube_no_cookie', 'default_upload_folder_create_new', 'default_upload_file_is_public'], 'boolean'],
            [['image_manipulation_library', 'default_upload_folder_filegroup_id', 'file_number_start_value', 'file_number_step_size'], 'integer'],
        ];
    }

    public function getFfmpegConfig(): array
    {
        return [//todo get path from settings
            //'ffmpeg.binaries'  => '/usr/local/bin/ffmpeg',
            //'ffprobe.binaries' => '/usr/local/bin/ffprobe',
            'ffmpeg.threads'   => 8
        ];
    }

    protected function pluginSettingDefinitions(): array
    {
        return [
            new PluginSetting($this, [
                'name' => 'image_manipulation_library',
                'label' => Yii::t('fafcms-core', 'Image manipulation library'),
                'inputType' => ExtendedDropDownList::class,
                'valueType' => FafcmsComponent::VALUE_TYPE_INT,
                'defaultValue' => 1,
                'items' => [
                    1 => 'Imagick',
                    2 => 'Gmagick',
                    3 => 'Gd',
                ],
                'projectBased' => false,
                'languageBased' => false,
            ]),
            new PluginSetting($this, [
                'name' => 'generate_images_on_the_fly',
                'label' => Yii::t('fafcms-filemanager', 'Generate images on the fly'),
                'inputType' => SwitchCheckbox::class,
                'valueType' => FafcmsComponent::VALUE_TYPE_BOOL,
                'defaultValue' => true,
                'fieldConfig' => [
                    'offLabel' => Yii::t('fafcms-core', 'No'),
                    'onLabel' => Yii::t('fafcms-core', 'Yes')
                ],
                'projectBased' => false,
                'languageBased' => false,
            ]),
            new PluginSetting($this, [
                'name' => 'force_youtube_no_cookie',
                'label' => Yii::t('fafcms-filemanager', 'Rewrite YouTube Videos URLs to youtube-nocookie.com.'),
                'inputType' => SwitchCheckbox::class,
                'valueType' => FafcmsComponent::VALUE_TYPE_BOOL,
                'defaultValue' => true,
                'fieldConfig' => [
                    'offLabel' => Yii::t('fafcms-core', 'No'),
                    'onLabel' => Yii::t('fafcms-core', 'Yes')
                ]
            ]),
            new PluginSetting($this, [
                'name' => 'file_variation_security_key',
                'label' => Yii::t('fafcms-filemanager', 'The security key to validate file variation requests.'),
                'inputType' => TextInput::class,
                'defaultValue' => static function() {
                    return Yii::$app->getSecurity()->generateRandomString(255);
                },
                'projectBased' => false,
                'languageBased' => false,
            ]),
            new PluginSetting($this, [
                'name' => 'default_upload_folder_create_new',
                'label' => Yii::t('fafcms-filemanager', 'Create new folder on upload.'),
                'inputType' => SwitchCheckbox::class,
                'valueType' => FafcmsComponent::VALUE_TYPE_BOOL,
                'defaultValue' => false,
                'fieldConfig' => [
                    'offLabel' => Yii::t('fafcms-core', 'No'),
                    'onLabel' => Yii::t('fafcms-core', 'Yes')
                ],
                'projectBased' => false,
                'languageBased' => false,
            ]),
            new PluginSetting($this, [
                'name' => 'default_upload_folder_new_path',
                'label' => Yii::t('fafcms-filemanager', 'Name or path of new folder on upload.'),
                'description' => Yii::t('fafcms-filemanager', 'Available placeholder data: fileUploadForm (instance of '.FileUploadForm::class.')'),
                'inputType' => Textarea::class,
                'valueType' => FafcmsComponent::VALUE_TYPE_TEXT,
                'projectBased' => false,
                'languageBased' => false
            ]),
            new PluginSetting($this, [
                'name' => 'default_upload_folder_filegroup_id',
                'label' => Yii::t('fafcms-filemanager', 'Preselected folder on upload.'),
                'inputType' => ExtendedDropDownList::class,
                'valueType' => FafcmsComponent::VALUE_TYPE_INT,
                'items' => static function() {
                    return Filegroup::instance()->getSelect(true)['items'];
                },
                'projectBased' => false,
                'languageBased' => false
            ]),
            new PluginSetting($this, [
                'name' => 'default_upload_filename_format',
                'label' => Yii::t('fafcms-filemanager', 'Format for file names on upload.'),
                'description' => Yii::t('fafcms-filemanager', 'Available placeholder data: fileName, fileNumber, filegroup (instance of '.Filegroup::class.'), filetype (instance of '.Filetype::class.'), fileUploadForm (instance of '.FileUploadForm::class.'), uploadedFile (instance of '.UploadedFile::class.')'),
                'inputType' => Textarea::class,
                'valueType' => FafcmsComponent::VALUE_TYPE_TEXT,
                'defaultValue' => '<fafcms-placeholder attribute="fileName" />',
                'projectBased' => false,
                'languageBased' => false
            ]),
            new PluginSetting($this, [
                'name' => 'file_number_start_value',
                'label' => Yii::t('fafcms-filemanager', 'Start value of file number.'),
                'inputType' => NumberInput::class,
                'valueType' => FafcmsComponent::VALUE_TYPE_INT,
                'defaultValue' => 1,
                'projectBased' => false,
                'languageBased' => false,
            ]),
            new PluginSetting($this, [
                'name' => 'file_number_step_size',
                'label' => Yii::t('fafcms-filemanager', 'Step size for file number.'),
                'inputType' => NumberInput::class,
                'valueType' => FafcmsComponent::VALUE_TYPE_INT,
                'defaultValue' => 1,
                'projectBased' => false,
                'languageBased' => false,
            ]),
            new PluginSetting($this, [
                'name' => 'default_upload_file_is_public',
                'label' => Yii::t('fafcms-filemanager', 'File access without login allowed'),
                'description' => Yii::t('fafcms-filemanager', 'Defines whether access to a file without login should be allowed by default when uploading files.'),
                'inputType' => SwitchCheckbox::class,
                'valueType' => FafcmsComponent::VALUE_TYPE_BOOL,
                'defaultValue' => true,
                'fieldConfig' => [
                    'offLabel' => Yii::t('fafcms-core', 'No'),
                    'onLabel' => Yii::t('fafcms-core', 'Yes')
                ],
                'projectBased' => false,
                'languageBased' => false,
            ]),
            new PluginSetting($this, [
                'name' => 'default_thumbnail_background_image',
                'label' => Yii::t('fafcms-filemanager', 'Default thumbnail background image'),
                'inputType' => FileSelect::class,
                'valueType' => FafcmsComponent::VALUE_TYPE_VARCHAR
            ]),
            new PluginSetting($this, [
                'name' => 'default_thumbnail_inner_background_image',
                'label' => Yii::t('fafcms-filemanager', 'Default thumbnail inner background image'),
                'inputType' => TextInput::class,
                'valueType' => FafcmsComponent::VALUE_TYPE_VARCHAR
            ]),
        ];
    }
}
