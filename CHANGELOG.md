[![Finally a fast CMS](https://www.finally-a-fast.com/logos/logo-cms-readme.jpg)](https://www.finally-a-fast.com/) | Changelog | Module file manager
================================================

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- Return type to prefixableTableName, rules and attributeLabels @cmoeke
- default_thumbnail_background_image setting @cmoeke
- default_thumbnail_inner_background_image setting @cmoeke

### Changed
- Unified save method @cmoeke
- Unified loadDefaultValues method @cmoeke
- Optimized input options of file input @cmoeke
- Regenerated models @cmoeke
- Changed db side timestamp to php DateTime format @cmoeke
- Column sizes for fomantic @cmoeke
- Changed menu items @cmoeke

### Fixed
- Bug of on the fly image generation @cmoeke
- Bug of missing file due to hardcoded inner-background-image value @cmoeke
- PDF Viewer iframe isnt working @cmoeke #22
- Thumbnails of PDFs are broken @cmoeke #21
- Static function editDataSingular @cmoeke
- Fixed file preview url for backend @cmoeke

### Removed
- Update scenario constant in File.php because it is now already defined in parent class @StefanBrandenburger

## [0.1.0] - 2020-09-29
### Added
- CHANGELOG.md @cmoeke fafcms-core#39
- Basic doc folder structure @cmoeke fafcms-core/37
- .gitlab-ci.yml for doc an build creation @cmoeke fafcms-core/38
- Info with possible file types at file upload modal @cmoeke fafcms-module-filemanager#18
- Info with max file size at file upload modal @cmoeke fafcms-module-filemanager#19
- Possibility to upload files to root folder @cmoeke
- Possibility to define step size of file number @cmoeke
- Possibility to define star value of file number @cmoeke
- Possibility to create new folder at upload of files by full path @cmoeke
- Possibility to define if always a folder should be created at uploads @cmoeke
- Possibility to define default path of new folder at file upload @cmoeke
- Possibility to define default target folder at file upload @cmoeke
- Possibility to define file format of uploaded files @cmoeke
- Added setting to define whether access to a file without login should be allowed by default when uploading files. @cmoeke
- Added width and height of pdfs to file meta data @cmoeke
- Added option to convert images only with cron jobs @cmoeke
- Added fix for files with old meta data @cmoeke
- Added option to convert svgs @cmoeke
- Added option to force image generation on the fly @cmoeke
- Added basic module config for ffmepg
- Added video duration and resolution to file meta

### Changed
- composer.json to use correct dependencies @cmoeke
- LICENSE to LICENSE.md @cmoeke
- Completely rewritten file upload @cmoeke
- Fixed output format of non web format images @cmoeke
- Improved meta data unserialization @cmoeke
- Improved performance by adding needed attributes to getAttributes method @cmoeke
- Improved performance by disabling change log of filesizevariation @cmoeke
- Improved performance by disabling validation of filesizevariation @cmoeke
- Improved performance by disabling change log of fileformatvariation @cmoeke
- Improved performance by disabling validation of fileformatvariation @cmoeke
- Improved performance by passing imagick object to imagine instead of using file blobs @cmoeke
- Moved replacements initialisation from bootstrapWebApp to bootstrapApp for console compatibility @StefanBrandenburger
- PHP dependency to 7.4 @cmoeke
- Changed ci config to handle new build branch @cmoeke

### Fixed
- Broken README.md icons @cmoeke fafcms-core#46
- Fixed bug when uploading more than 10 files at once @cmoeke fafcms-module-filemanager#5
- Fixed bug when login is needed to view a file @cmoeke
- Fixed bug of wrong concatenation of src sets @cmoeke
- Fixed image generation of pdfs when its also defined as background image @cmoeke
- Fixed broken colorspace bug @cmoeke
- Fixed composer replace conflict at ci build @cmoeke

[Unreleased]: https://gitlab.com/finally-a-fast/fafcms-module-filemanager/-/tree/master
[0.1.0]: https://gitlab.com/finally-a-fast/fafcms-module-filemanager/-/tree/v0.1.0-alpha
